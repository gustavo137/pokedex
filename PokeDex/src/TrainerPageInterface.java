import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JTextField;
import javax.swing.SpringLayout;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.GridBagLayout;
import javax.swing.JPanel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class TrainerPageInterface extends BackgroundDrawer {
	
	Trainer trainer = new Trainer();

	private JFrame frame;

	public TrainerPageInterface() {
		//initialize();
	}
	
	public void showPage(boolean option, Trainer trainer) {
		
		this.trainer = trainer;
		
		if (option == true) {
			initialize();
			frame.setVisible(true);
		}else
		if (option == false) {
			initialize();
			frame.setVisible(false);
		}
			
	}

	/**
	 * @wbp.parser.entryPoint
	 */
	private void initialize() {
		
    	System.out.println("Treinador logado nessa sessao : ");
    	System.out.println(trainer.getUserName());
    	System.out.println(trainer.getPassWord());
    	System.out.println(trainer.getName());
    	System.out.println(trainer.getGender());
		
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JButton btnPokedex = new JButton("Pokedex");
		btnPokedex.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PokedexInterface newInterface = new PokedexInterface();
				newInterface.setLocalTrainer(trainer);
				newInterface.showPage(true);
				frame.setVisible(false);
			}
		});
		btnPokedex.setBounds(28, 44, 117, 25);
		frame.getContentPane().add(btnPokedex);
		
		JButton btnRegisterPokemon = new JButton("Register Pokemon");
		btnRegisterPokemon.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PokemonRegistrationInterface newPokemonRegistration = new PokemonRegistrationInterface();
				newPokemonRegistration.setLocalTrainer(trainer);
				newPokemonRegistration.showPage(true);
				frame.setVisible(false);
			}
		});
		
		btnRegisterPokemon.setBounds(28, 103, 163, 25);
		frame.getContentPane().add(btnRegisterPokemon);
		
		JButton btnMyBox = new JButton("My Box");
		btnMyBox.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				PokemonBoxInterface newPokemonBoxInterface = new PokemonBoxInterface();//not corrected
				newPokemonBoxInterface.setLocalTrainer(trainer);
				newPokemonBoxInterface.showPage(true);
				frame.setVisible(false);
			}
			
		});
		btnMyBox.setBounds(28, 154, 117, 25);
		frame.getContentPane().add(btnMyBox);
		
		JLabel MenuHolder = new JLabel("");
		MenuHolder.setBounds(12, 28, 215, 260);
		frame.getContentPane().add(MenuHolder);
		
		String holderLocation = ("pokedexImages/MenuHolderRegistration.png");
		Image img = new ImageIcon (this.getClass().getResource(holderLocation)).getImage();
		MenuHolder.setIcon(new ImageIcon(img));
		frame.getContentPane().add(MenuHolder);
		
		JLabel Growlithe = new JLabel("");
		Growlithe.setBounds(238, 55, 200, 200);
		frame.getContentPane().add(Growlithe);
		
		String imageLocation = ("pokedexImages/Growlithe.gif");
		Image imge = new ImageIcon (this.getClass().getResource(imageLocation)).getImage();
		Growlithe.setIcon(new ImageIcon(imge));
		frame.getContentPane().add(Growlithe);
		
		
		
		this.drawBackground(frame);
		
		frame.setVisible(true);
	}

	public Trainer getLocalTrainer() {
		return trainer;
	}

	public void setLocalTrainer(Trainer localTrainer) {
		this.trainer = localTrainer;
	}
}
