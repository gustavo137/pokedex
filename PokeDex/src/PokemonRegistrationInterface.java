import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.JFrame;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class PokemonRegistrationInterface extends BackgroundDrawer {
	
	Trainer localTrainer = new Trainer();
	private JFrame frame;
	private JTextField textField;
	

	public PokemonRegistrationInterface() {
	
	}
	
	public void showPage(boolean option) {
		if (option == true) {
			initialize();
			frame.setVisible(true);
		}else
		if (option == false) {
			initialize();//pode bugar 
			frame.setVisible(false);
		}
			
	}

	private void initialize() {
		
    	System.out.println("Treinador logado nesse registro : ");
    	System.out.println(localTrainer.getUserName());
    	System.out.println(localTrainer.getPassWord());
    	System.out.println(localTrainer.getName());
    	System.out.println(localTrainer.getGender());
    	
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		
		JButton btnNewButton = new JButton("Back");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TrainerPageInterface newTrainerPage = new TrainerPageInterface();
				newTrainerPage.setLocalTrainer(localTrainer);
				newTrainerPage.showPage(true,localTrainer);
				frame.setVisible(false);
			}
		});
		btnNewButton.setBounds(39, 204, 117, 25);
		frame.getContentPane().add(btnNewButton);
		
		JLabel lblPokemonRegistration = new JLabel("Pokemon Registration");
		lblPokemonRegistration.setBounds(39, 29, 172, 15);
		frame.getContentPane().add(lblPokemonRegistration);
		
		JLabel lblName = new JLabel("name");
		lblName.setBounds(39, 81, 70, 15);
		frame.getContentPane().add(lblName);
		
		textField = new JTextField();
		textField.setBounds(97, 79, 114, 19);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JButton btnRegister = new JButton("Register");
		btnRegister.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PokemonRegistrator localRegistrator = new PokemonRegistrator();
				
				localRegistrator.registerPokemon(localTrainer.getUserName(), textField.getText());
				if (localRegistrator.checkPokemon(textField.getText())) 
				{
					JOptionPane.showMessageDialog(frame, "Pokemon Registered");
					textField.setText("");
				}
				else 
				{
					JOptionPane.showMessageDialog(frame, "Pokemon Not Found");
					textField.setText("");
				}
			}
		});
		btnRegister.setBounds(39, 167, 117, 25);
		frame.getContentPane().add(btnRegister);
		
		JLabel MenuHolder = new JLabel("");
		MenuHolder.setBounds(24, 18, 215, 260);
		frame.getContentPane().add(MenuHolder);
		
		String holderLocation = ("pokedexImages/MenuHolderRegistration.png");
		Image img = new ImageIcon (this.getClass().getResource(holderLocation)).getImage();
		MenuHolder.setIcon(new ImageIcon(img));
		frame.getContentPane().add(MenuHolder);
		
		this.drawBackground(frame);
		frame.setVisible(true);
		
	}
	
	

	public Trainer getLocalTrainer() {
		return localTrainer;
	}

	public void setLocalTrainer(Trainer localTrainer) {
		this.localTrainer = localTrainer;
	}
}
