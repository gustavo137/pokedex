import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.table.DefaultTableModel;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.Vector;
import java.awt.event.ActionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;

public class PokemonBoxInterface extends BackgroundDrawer {

	private JFrame frame;
	Trainer localTrainer = new Trainer();
	Vector<Pokemon> pokemonsOnThisBox = new Vector<Pokemon>();
	PokemonRegistrator localRegistrator = new PokemonRegistrator();
	
	//Trainer localTrainer = new Trainer();
	PokemonCsvManager localManager = new PokemonCsvManager();
	Pokemon localPokemon = new Pokemon();

	public PokemonBoxInterface() {
		
	}
	
	public void showPage(boolean option) {
		if (option == true) {
			initialize();
			frame.setVisible(true);
		}else
		if (option == false) {
			initialize();
			frame.setVisible(false);
		}
			
	}


	private void initialize() {
		
		localRegistrator.getRegisteredPokemons(localTrainer.getUserName());
		pokemonsOnThisBox = localRegistrator.getRegisteredPokemons(localTrainer.getUserName());
		
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 0, 0);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel PokemonLabel = new JLabel("");
		PokemonLabel.setBounds(291, 68, 100, 100);
		frame.getContentPane().add(PokemonLabel);
		
		JLabel lblPokemons = new JLabel("Pokemons");
		lblPokemons.setBounds(12, 17, 119, 26);
		panel.add(lblPokemons);
		
		DefaultListModel<String> dlm = new DefaultListModel<String>();
		
		PokemonCsvManager newManager = new PokemonCsvManager(); 
		newManager.registerPokemons();                          
		
		for (int i=0 ; i < pokemonsOnThisBox.size()/2 ; i++)
		{	
			System.out.println("pokemon " + i + "registered on list");
			dlm.addElement(pokemonsOnThisBox.get(i).getName());
		}
		
		System.out.println(pokemonsOnThisBox.size());
		
		JList<String> list = new JList<String>(dlm);
		list.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent arg0) {
				
				localManager.registerPokemons();
				localPokemon = localManager.pokemonSearchByName(list.getSelectedValue().toString().intern());
				
				String imagesLocation_character = localPokemon.getImageName();//("pokedexImages/PokedexInterface.png");
				System.out.println(imagesLocation_character);
				Image img_character = new ImageIcon (this.getClass().getResource("images/" + imagesLocation_character)).getImage();
				PokemonLabel.setIcon(new ImageIcon(img_character));
				
				//
			}
		});
		list.setBounds(12, 55, 119, 218);
		panel.add(list);
		Color newColor = new Color(240, 216, 112);
		list.setBackground(newColor);
		
		JScrollPane scrollPane = new JScrollPane(list);
		scrollPane.setBounds(0, 0, 227, 300);
		frame.getContentPane().add(scrollPane);
		
		JButton btnMenu = new JButton("Menu");
		btnMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TrainerPageInterface newTrainerPage = new TrainerPageInterface();
				newTrainerPage.showPage(true,localTrainer);
				frame.setVisible(false);
			}
		});
		btnMenu.setBounds(239, 224, 117, 25);
		frame.getContentPane().add(btnMenu);
		this.drawSideDecorations(frame);
		this.drawBackground(frame);
		frame.setVisible(true);
	}

	public Trainer getLocalTrainer() {
		return localTrainer;
	}

	public void setLocalTrainer(Trainer localTrainer) {
		this.localTrainer = localTrainer;
	}
}
