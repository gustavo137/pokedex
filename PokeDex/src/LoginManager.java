import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Vector;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;


public class LoginManager {
	
	Vector<Trainer> trainersVector = new Vector<>();
	
	int numberOfRegisteredTrainers;
	
	public Trainer loginCheck (String givenUsername , String givenPassword)
	{
		Trainer localTrainer= new Trainer();
		trainersVector.add(localTrainer);
		return localTrainer;
	}
	
	public Trainer readDataFiles(String givenUsername,String givenPassword) 
	{	
		Trainer thisTrainer = new Trainer();
		boolean userFound = false;
		
		String first = "";
		String second = "";
		
		String localUsername = "";
		String localPassWord = "";
		String localName = "";
		String localGender = "";
		
        String fileName= "data/csv_files/Logins.csv";
        
        File file= new File(fileName);

        List<List<String>> lines = new ArrayList<>();
        Scanner inputStream;

        try{
            inputStream = new Scanner(file);

            while(inputStream.hasNext()){
                String line= inputStream.next();
                String[] values = line.split(",");
                lines.add(Arrays.asList(values));
            }

            inputStream.close();
        }catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        int lineNo = 1;
        
        for(List<String> line: lines) 
        {        	
            int columnNo = 1;
        	Trainer localTrainer = new Trainer();

            for (String value: line) 
            {	            	
            	if (lineNo > 1) 
            	{
                  if (columnNo == 1) 
                  {
                    //System.out.println("Username : " + value);
                    localTrainer.setUserName(value);
                  }
                  if (columnNo == 2)
                  {
                	//System.out.println("PassWord : " + value);
                    localTrainer.setPassWord(value);
                  }
                  if (columnNo == 3) 
                  {
                    //System.out.println("Name : " + value);
                    localTrainer.setName(value);
                  }
                  if (columnNo == 4)
                  {
                	//System.out.println("Gender : " + value);
                    localTrainer.setGender(value);
                    
                  }
                }
                columnNo++;
            }
            lineNo++;
            
        	trainersVector.add(localTrainer);
        	
        	System.out.println("-------------------");
        	System.out.println(localTrainer.getUserName());
        	System.out.println(localTrainer.getPassWord());
        	System.out.println(localTrainer.getName());
        	System.out.println(localTrainer.getGender());
        	System.out.println("-------------------");

            if (givenUsername.equals(localTrainer.getUserName()) && givenPassword.equals(localTrainer.getPassWord())) 
            {
            	thisTrainer = localTrainer;
            	userFound = true;
        		System.out.println("Data is correct - login acepted");
            }
            
        }
        
    return thisTrainer;
    
    }
	
	public boolean registerOnFile (String username, String password , String name , String gender) 
	{
        String fileName= "/home/gustavoafonso/git/PokedexLocalRepository/PokeDex/src/Logins.csv";
		Writer output;
		try {
		output = new BufferedWriter(new FileWriter(fileName,true));
		output.append("\n"+username+","+password+","+ name+","+ gender);
		output.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		finally 
		{
			
		}
		return true;
	}
}