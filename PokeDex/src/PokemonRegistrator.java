import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.io.File;
import java.util.Vector;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Vector;
import java.io.FileWriter;


public class PokemonRegistrator {
	
	Vector<Pokemon> pokemonsOwned = new Vector<Pokemon>();
    String fileName= "data/csv_files/RegisteredPokemons.csv";
    PokemonCsvManager localManager = new PokemonCsvManager();

	public void registerPokemon(String trainerName , String pokemonName)
	{
		Pokemon localPokemon = new Pokemon();
		PokemonCsvManager localManager = new PokemonCsvManager();
		localManager.registerPokemons();
		localPokemon = localManager.pokemonSearchByName(pokemonName);
		
		Writer output;
		
		try 
		{
		output = new BufferedWriter(new FileWriter(fileName,true));
		output.append("\n" + trainerName + "," + localPokemon.getName());
		output.close();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		finally 
		{
		
		}
	}
	
	public boolean checkPokemon(String name) 
	{
		boolean condition = false;
		PokemonCsvManager localManager = new PokemonCsvManager();
		localManager.registerPokemons();
		if(localManager.pokemonSearchByName(name).getName() == null ) {
		condition = false;
		}else {
		condition = true;
		}
		return condition;
	}
	
	public Vector<Pokemon> getRegisteredPokemons(String trainerName) 
	{                                                      		        
        File file = new File(fileName);
        localManager.registerPokemons();
        

        List<List<String>> lines = new ArrayList<>();
        Scanner inputStream;

        try{
        	
            inputStream = new Scanner(file);

            while(inputStream.hasNext()){
                String line= inputStream.next();
                String[] values = line.split(",");
                lines.add(Arrays.asList(values));
            }

            inputStream.close();
        }catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        
    	Pokemon pokemonToRegister = new Pokemon();
        boolean register = false;        	
        int lineNo = 1;
        
        for(List<String> line: lines) 
        {
            int columnNo = 1;
            for (String value: line) 
            {
            	if (register == true) 
            	{
            		pokemonsOwned.add(localManager.pokemonSearchByName(value));
            		register = false;
            	}
            	
            	if (trainerName.equals(value)) // if trainer name == to the first line it will register the next. 
            	{
            		register = true;
            	}

            	//System.out.println(value);
            	
                columnNo++; 
            }
            lineNo++;
        }
        //pokemonsListed.add(pokemonToRegister);
    for (int i = 0 ; i < pokemonsOwned.size();i++) {
        System.out.println(i);
    }
    
    return pokemonsOwned;   
	}
}