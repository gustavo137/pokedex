public class Pokemon {
	int id;
	String name;
	String type_1;
	String Type_2;
	int xp;
	int Total;
	int HP;
	int Attack;
	int Defense;
	int Sp_Atk;
	int Sp_Def;
	int Speed;
    int Generation;
	String Legendary;
	int Height;
	int Weight; 
	String Abilitie_1;
	String Abilitie_2;
	String Abilitie_3;
	String Move_1;
	String Move_2;
	String Move_3;
	String Move_4;
	String Move_5;
	String Move_6;
	String Move_7;
	
	String imageName;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType_1() {
		return type_1;
	}
	public void setType_1(String type_1) {
		this.type_1 = type_1;
	}
	public String getType_2() {
		return Type_2;
	}
	public void setType_2(String type_2) {
		Type_2 = type_2;
	}
	public int getXp() {
		return xp;
	}
	public void setXp(int xp) {
		this.xp = xp;
	}
	public int getTotal() {
		return Total;
	}
	public void setTotal(int total) {
		Total = total;
	}
	public int getHP() {
		return HP;
	}
	public void setHP(int hP) {
		HP = hP;
	}
	public int getAttack() {
		return Attack;
	}
	public void setAttack(int attack) {
		Attack = attack;
	}
	public int getDefense() {
		return Defense;
	}
	public void setDefense(int defense) {
		Defense = defense;
	}
	public int getSp_Atk() {
		return Sp_Atk;
	}
	public void setSp_Atk(int sp_Atk) {
		Sp_Atk = sp_Atk;
	}
	public int getSp_Def() {
		return Sp_Def;
	}
	public void setSp_Def(int sp_Def) {
		Sp_Def = sp_Def;
	}
	public int getSpeed() {
		return Speed;
	}
	public void setSpeed(int speed) {
		Speed = speed;
	}
	public int getGeneration() {
		return Generation;
	}
	public void setGeneration(int generation) {
		Generation = generation;
	}
	public String isLegendary() {
		return Legendary;
	}
	public void setLegendary(String legendary) {
		Legendary = legendary;
	}
	public int getHeight() {
		return Height;
	}
	public void setHeight(int height) {
		Height = height;
	}
	public int getWeight() {
		return Weight;
	}
	public void setWeight(int weight) {
		Weight = weight;
	}
	public String getAbilitie_1() {
		return Abilitie_1;
	}
	public void setAbilitie_1(String abilitie_1) {
		Abilitie_1 = abilitie_1;
	}
	public String getAbilitie_2() {
		return Abilitie_2;
	}
	public void setAbilitie_2(String abilitie_2) {
		Abilitie_2 = abilitie_2;
	}
	public String getAbilitie_3() {
		return Abilitie_3;
	}
	public void setAbilitie_3(String abilitie_3) {
		Abilitie_3 = abilitie_3;
	}
	public String getMove_1() {
		return Move_1;
	}
	public void setMove_1(String move_1) {
		Move_1 = move_1;
	}
	public String getMove_2() {
		return Move_2;
	}
	public void setMove_2(String move_2) {
		Move_2 = move_2;
	}
	public String getMove_3() {
		return Move_3;
	}
	public void setMove_3(String move_3) {
		Move_3 = move_3;
	}
	public String getMove_4() {
		return Move_4;
	}
	public void setMove_4(String move_4) {
		Move_4 = move_4;
	}
	public String getMove_5() {
		return Move_5;
	}
	public void setMove_5(String move_5) {
		Move_5 = move_5;
	}
	public String getMove_6() {
		return Move_6;
	}
	public void setMove_6(String move_6) {
		Move_6 = move_6;
	}
	public String getMove_7() {
		return Move_7;
	}
	public void setMove_7(String move_7) {
		Move_7 = move_7;
	}
	public String getImageName() {
		return imageName;
	}
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
}
