import java.awt.EventQueue;
import java.awt.Image;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class RegistrationPageInterface extends BackgroundDrawer {

	private JFrame frame;
	private JTextField textField;
	private JLabel lblPassword;
	private JPasswordField passwordField;
	LoginManager newManager = new LoginManager();
	Trainer localTrainer = new Trainer();
	private JTextField nameTextField;

	public RegistrationPageInterface() {

	}
	
	public void showPage(boolean option) {
		if (option == true) {
			initialize();
			frame.setVisible(true);
		}else
		if (option == false) {
			initialize();
			frame.setVisible(false);
		}
			
	}

	/**
	 * @wbp.parser.entryPoint
	 */
	private void initialize() {
		
		
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblName = new JLabel("UserName:");
		lblName.setBounds(28, 33, 86, 15);
		frame.getContentPane().add(lblName);
		
		textField = new JTextField();
		textField.setBounds(115, 33, 114, 19);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		lblPassword = new JLabel("PassWord");
		lblPassword.setBounds(28, 65, 86, 15);
		frame.getContentPane().add(lblPassword);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(115, 64, 114, 19);
		frame.getContentPane().add(passwordField);
		
		JLabel lblName_1 = new JLabel("Name:");
		lblName_1.setBounds(44, 95, 70, 15);
		frame.getContentPane().add(lblName_1);
		
		nameTextField = new JTextField();
		nameTextField.setBounds(115, 95, 114, 19);
		frame.getContentPane().add(nameTextField);
		nameTextField.setColumns(10);
		
		JLabel lblGender = new JLabel("Gender:");
		lblGender.setBounds(28, 126, 70, 15);
		frame.getContentPane().add(lblGender);
		
		JRadioButton rdbtnFemale = new JRadioButton("Female");
		JRadioButton rdbtnMale = new JRadioButton("Male");
		
		rdbtnFemale.setOpaque(false);
		rdbtnMale.setOpaque(false);
		
		rdbtnFemale.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				rdbtnMale.setSelected(false);
				rdbtnFemale.setSelected(true);
				//drawFemaleCharacter();
			}
		});
		
		rdbtnMale.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				rdbtnMale.setSelected(true);
				rdbtnFemale.setSelected(false);
				//drawMaleCharacter();
			}
			
		});
		
		rdbtnMale.setBounds(47, 151, 70, 23);
		frame.getContentPane().add(rdbtnMale);
		
		rdbtnFemale.setBounds(47, 178, 86, 23);
		frame.getContentPane().add(rdbtnFemale);
		
		JButton btnConfirm = new JButton("Confirm");
		btnConfirm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String username = textField.getText();
				String password = passwordField.getText();
				String name = nameTextField.getText();
				String gender = "";
				
				if (rdbtnMale.isSelected() == true) 
				{
					gender = "male";
				}
				if (rdbtnFemale.isSelected() == true)  
				{
					gender = "female";
				}
				
				if (username.equals("") || passwordField.equals("") || nameTextField.equals("") || ( (rdbtnMale.isSelected() == false) && (rdbtnFemale.isSelected() == false)) ) 
				{
					JOptionPane.showMessageDialog(frame, "Invalid Fields");
				}else
				
				if (newManager.registerOnFile(username , password , name , gender ) == true)
		        {
					JOptionPane.showMessageDialog(frame, "Registration Complete");
					LoginPageInterface newLoginPage = new LoginPageInterface();
					newLoginPage.showPage(true);
					frame.setVisible(false);
		        }
				
			}
		});
		btnConfirm.setBounds(47, 210, 117, 25);
		frame.getContentPane().add(btnConfirm);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				LoginPageInterface newLoginPage = new LoginPageInterface();
				newLoginPage.showPage(true);
				frame.setVisible(false);
				
			}
		});
		btnBack.setBounds(47, 239, 117, 25);
		frame.getContentPane().add(btnBack);
		
		JLabel trainerLabel = new JLabel("");
		trainerLabel.setBounds(228, 44, 240, 240);
		frame.getContentPane().add(trainerLabel);
		String imagesLocation_character = ("pokedexImages/MaleCharacterFullBody.png");
		System.out.println(imagesLocation_character);
		Image img_character = new ImageIcon (this.getClass().getResource(imagesLocation_character)).getImage();
		trainerLabel.setIcon(new ImageIcon(img_character));
		frame.getContentPane().add(trainerLabel);
		
		JLabel MenuHolder = new JLabel("");
		MenuHolder.setBounds(22, 24, 215, 260);
		frame.getContentPane().add(MenuHolder);
		
		String holderLocation = ("pokedexImages/MenuHolderRegistration.png");
		Image img = new ImageIcon (this.getClass().getResource(holderLocation)).getImage();
		MenuHolder.setIcon(new ImageIcon(img));
		frame.getContentPane().add(MenuHolder);
		
		this.drawBackground(frame);
		
		frame.setVisible(true);
	}
	
	private void drawMaleCharacter() {
		
		JLabel trainerLabel = new JLabel("");
		trainerLabel.setBounds(198, 24, 240, 240);
		frame.getContentPane().add(trainerLabel);
		String imagesLocation_character = ("pokedexImages/MaleCharacterFullBody.png");
		System.out.println(imagesLocation_character);
		Image img_character = new ImageIcon (this.getClass().getResource(imagesLocation_character)).getImage();
		trainerLabel.setIcon(new ImageIcon(img_character));
		frame.getContentPane().add(trainerLabel);
		
	}
	private void drawFemaleCharacter() {
		
		JLabel trainerLabel = new JLabel("");
		trainerLabel.setBounds(198, 24, 240, 240);
		frame.getContentPane().add(trainerLabel);
		String imagesLocation_character = ("pokedexImages/FemaleCharacterFullBody.png");
		System.out.println(imagesLocation_character);
		Image img_character = new ImageIcon (this.getClass().getResource(imagesLocation_character)).getImage();
		trainerLabel.setIcon(new ImageIcon(img_character));
		frame.getContentPane().add(trainerLabel);
		
	}
}
