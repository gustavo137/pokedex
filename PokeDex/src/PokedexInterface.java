
import java.awt.EventQueue;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JList;
import java.awt.GridLayout;
import java.awt.Image;

import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import javax.swing.AbstractListModel;
import javax.swing.JLabel;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;

public class PokedexInterface extends BackgroundDrawer  {
	
	Trainer localTrainer = new Trainer();
	PokemonCsvManager localManager = new PokemonCsvManager();
	Pokemon localPokemon = new Pokemon();
	
	private JFrame frame;
	
	public PokedexInterface() {
		//initialize();
	}
	
	public void showPage(boolean option) {
		if (option == true) {
			initialize();
			frame.setVisible(true);
		}else
		if (option == false) {
			initialize();//pode bugar 
			frame.setVisible(false);
		}
			
	}

	/**
	 * @wbp.parser.entryPoint
	 */
	private void initialize() {
		
    	System.out.println("Treinador logado nessa Pokedex : ");
    	System.out.println(localTrainer.getUserName());
    	System.out.println(localTrainer.getPassWord());
    	System.out.println(localTrainer.getName());
    	System.out.println(localTrainer.getGender());
		
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 0, 0);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel PokemonLabel = new JLabel("");
		PokemonLabel.setBounds(291, 68, 100, 100);
		frame.getContentPane().add(PokemonLabel);
		
			
		JLabel lblPokemons = new JLabel("Pokemons");
		lblPokemons.setBounds(12, 17, 119, 26);
		panel.add(lblPokemons);
		
		DefaultListModel<String> dlm = new DefaultListModel<String>();
		
		PokemonCsvManager newManager = new PokemonCsvManager();
		newManager.registerPokemons();
		
		for (int i=0 ; i<721 ; i++) 
		{	
			newManager.getSpecifiedPokemon(i);
			dlm.addElement(newManager.getSpecifiedPokemon(i).getName());
		}
		
		JList<String> list = new JList<String>(dlm);
		list.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent arg0) {
				
				localManager.registerPokemons();
				localPokemon = localManager.pokemonSearchByName(list.getSelectedValue().toString().intern());
				
				String imagesLocation_character = localPokemon.getImageName();//("pokedexImages/PokedexInterface.png");
				System.out.println(imagesLocation_character);
				Image img_character = new ImageIcon (this.getClass().getResource("images/" + imagesLocation_character)).getImage();
				PokemonLabel.setIcon(new ImageIcon(img_character));
				
				//
			}
		});
		Color newColor = new Color(240, 216, 112);
		list.setBackground(newColor);
		list.setBounds(12, 55, 119, 218);
		panel.add(list);
		
		JScrollPane scrollPane = new JScrollPane(list);
		scrollPane.setBackground(newColor);
		scrollPane.setBounds(0, 0, 227, 300);
		
		frame.getContentPane().add(scrollPane);
		
		JButton btnSearch = new JButton("Advanced Search");
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				searchPageInterface newAdvancedSearch = new searchPageInterface();
				newAdvancedSearch.setLocalTrainer(localTrainer);
				newAdvancedSearch.showPage(true);
				frame.setVisible(false);
			}
		});
		btnSearch.setBounds(258, 191, 155, 25);
		frame.getContentPane().add(btnSearch);
		
		JButton btnMenu = new JButton("Menu");
		btnMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TrainerPageInterface newTrainerPage = new TrainerPageInterface();
				newTrainerPage.showPage(true,localTrainer);
				frame.setVisible(false);
			}
		});
		btnMenu.setBounds(258, 228, 117, 25);
		frame.getContentPane().add(btnMenu);
		
		JLabel SideDecoration = new JLabel("");
		SideDecoration.setBounds(225, 0, 225, 300);
		frame.getContentPane().add(SideDecoration);
		
		String imagesLocation_character = ("pokedexImages/PokedexInterface.png");
		System.out.println(imagesLocation_character);
		Image img_character = new ImageIcon (this.getClass().getResource(imagesLocation_character)).getImage();
		SideDecoration.setIcon(new ImageIcon(img_character));
		frame.getContentPane().add(SideDecoration);
		
		
		this.drawBackground(frame);
		frame.setVisible(true);
	}

	public Trainer getLocalTrainer() {
		return localTrainer;
	}

	public void setLocalTrainer(Trainer localTrainer) {
		this.localTrainer = localTrainer;
	}
}//pokedexImages/RisghtSide.png
