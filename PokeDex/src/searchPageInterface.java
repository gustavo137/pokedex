import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JSeparator;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class searchPageInterface extends BackgroundDrawer{
	Trainer localTrainer = new Trainer();
	private JFrame frame;
	private JTextField textField;
	PokemonCsvManager newManager = new PokemonCsvManager();

	public searchPageInterface() {

	}
	
	public void showPage(boolean option) {
		if (option == true) {
			initialize();
			frame.setVisible(true);
		}else
		if (option == false) {
			initialize();
			frame.setVisible(false);
		}
			
	}

	/**
	 * @wbp.parser.entryPoint
	 */
	private void initialize() {
		
    	System.out.println("Treinador logado nessa Busca avançada : ");
    	System.out.println(localTrainer.getUserName());
    	System.out.println(localTrainer.getPassWord());
    	System.out.println(localTrainer.getName());
    	System.out.println(localTrainer.getGender());
    	
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblName = new JLabel("Name:");
		lblName.setBounds(242, 99, 70, 15);
		frame.getContentPane().add(lblName);
		
		textField = new JTextField();
		textField.setBounds(311, 97, 114, 19);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JButton btnApplySearch = new JButton("Apply Search");
		btnApplySearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String nameToFind = textField.getText();
				Pokemon newPokemon = new Pokemon();
				newManager.registerPokemons();
				newPokemon = newManager.pokemonSearchByName(nameToFind);
				PokemonInformationsInterface newPokemonPageInterface = new PokemonInformationsInterface();
				newPokemonPageInterface.setLocalTrainer(localTrainer);
				newPokemonPageInterface.showPage(true,newPokemon);
				frame.setVisible(false);
			}
		});
		btnApplySearch.setBounds(242, 148, 132, 25);
		frame.getContentPane().add(btnApplySearch);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				PokedexInterface newInterface = new PokedexInterface();
				newInterface.setLocalTrainer(localTrainer);
				newInterface.showPage(true);
				frame.setVisible(false);
			}
		});
		btnBack.setBounds(242, 199, 117, 25);
		frame.getContentPane().add(btnBack);
		
		JLabel MenuHolder = new JLabel("");
		MenuHolder.setBounds(223, 28, 215, 260);
		frame.getContentPane().add(MenuHolder);
		
		String holderLocation = ("pokedexImages/MenuHolderRegistration.png");
		Image img = new ImageIcon (this.getClass().getResource(holderLocation)).getImage();
		MenuHolder.setIcon(new ImageIcon(img));
		frame.getContentPane().add(MenuHolder);
		
		JLabel Phantump = new JLabel("");
		Phantump.setBounds(12, 62, 200, 200);
		frame.getContentPane().add(Phantump);
		
		String imageLocation = ("pokedexImages/phantump.gif");
		Image imge = new ImageIcon (this.getClass().getResource(imageLocation)).getImage();
		Phantump.setIcon(new ImageIcon(imge));
		frame.getContentPane().add(Phantump);
		
		this.drawBackground(frame);
		frame.setVisible(true);
	}

	public Trainer getLocalTrainer() {
		return localTrainer;
	}

	public void setLocalTrainer(Trainer localTrainer) {
		this.localTrainer = localTrainer;
	}
}
