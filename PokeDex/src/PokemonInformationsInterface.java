import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Image;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import java.awt.*;
import java.awt.image.*;
import java.io.*;
import javax.swing.*;
import javax.imageio.ImageIO;

public class PokemonInformationsInterface extends BackgroundDrawer{
	Trainer localTrainer = new Trainer();
	private JFrame frame;

	public void showPage(boolean option, Pokemon pokemon) {
		if (option == true) {
			initialize(pokemon);
			frame.setVisible(true);
		}else
		if (option == false) {
			initialize(pokemon);
			frame.setVisible(false);
		}
			
	}

	private void initialize(Pokemon pokemon) {
		
		PokemonCsvManager newManager = new PokemonCsvManager(); 
		newManager.registerPokemons();
		newManager.pokemonSearchByName("Cartepie");
		
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 0, 0);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel lblPokemons = new JLabel("Pokemons");
		lblPokemons.setBounds(12, 17, 119, 26);
		panel.add(lblPokemons);
		
		DefaultListModel<String> dlm = new DefaultListModel<String>();

		dlm.addElement("name: " + pokemon.getName());
		dlm.addElement("ID: " + pokemon.getId());
		dlm.addElement("Type 1: " + pokemon.getType_1());
		dlm.addElement("Type 2: " + pokemon.getType_2());
		dlm.addElement("Total: " + pokemon.getTotal());
		dlm.addElement("Hp: " + pokemon.getHP());
		dlm.addElement("Attack: " + pokemon.getAttack());
		dlm.addElement("Defense " + pokemon.getDefense());
		dlm.addElement("Sp_Atk: " + pokemon.getSp_Atk());
		dlm.addElement("Sp_Def: " + pokemon.getSp_Def());
		dlm.addElement("Speed: " + pokemon.getSpeed());
		dlm.addElement("Generation: " + pokemon.getGeneration());
		dlm.addElement("Legendary: " + pokemon.isLegendary());
		
		JList<String> list = new JList<String>(dlm);
		Color newColor = new Color(240, 216, 112);
		list.setBackground(newColor);
		list.setBounds(12, 55, 119, 218);
		panel.add(list);
		
		JScrollPane scrollPane = new JScrollPane(list);
		scrollPane.setBounds(0, 0, 227, 300);
		frame.getContentPane().add(scrollPane);
		
		JButton btnMenu = new JButton("Back");
		btnMenu.setBounds(274, 207, 73, 25);
		frame.getContentPane().add(btnMenu);
		
		JLabel Label = new JLabel("");
		String imagesLocation = ("images/" + pokemon.getImageName()); //caterpie.png";
		System.out.println(imagesLocation);
		Image img = new ImageIcon (this.getClass().getResource(imagesLocation)).getImage();
		Label.setIcon(new ImageIcon(img));
		Label.setBounds(292, 26, 124, 169);
		frame.getContentPane().add(Label);
		
		
		btnMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				searchPageInterface newAdvancedSearch = new searchPageInterface();
				newAdvancedSearch.setLocalTrainer(localTrainer);
				newAdvancedSearch.showPage(true);
				frame.setVisible(false);
			}
		});
		this.drawSideDecorations(frame);
		this.drawBackground(frame);
		frame.setVisible(true);
	}

	public Trainer getLocalTrainer() {
		return localTrainer;
	}

	public void setLocalTrainer(Trainer localTrainer) {
		this.localTrainer = localTrainer;
	}
}
