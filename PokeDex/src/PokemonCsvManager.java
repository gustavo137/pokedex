import java.io.File;
import java.util.Vector;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Vector;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;


public class PokemonCsvManager {
	
	Vector<Pokemon> pokemonsListed = new Vector<Pokemon>(721);
	
	public void registerPokemons() 
	{
		int selected = 746;
		
		String local_id = "";
		String local_name = "";
		String local_type_1 = "";
		String local_type_2 = "";
		String local_xp = "";
		String local_Total  = "";
		String local_HP  = "";
		String local_Attack  = "";
		String local_Defense  = "";
		String local_Sp_Atk  = "";
		String local_Sp_Def  = "";
		String local_Speed  = "";
		String local_Generation  = "";
		String local_Legendary  = "";
		String local_Height  = "";
		String local_Weight  = "";
		String local_Abilitie_1  = "";
		String local_Abilitie_2 = "";
		String local_Abilitie_3  = "";
		String local_Move_1  = "";
		String local_Move_2  = "";
		String local_Move_3  = "";
		String local_Move_4  = "";
		String local_Move_5  = "";
		String local_Move_6  = "";
		String local_Move_7  = "";
		
        String fileName= "data/csv_files/POKEMONS_DATA_1.csv";
        
        File file= new File(fileName);

        List<List<String>> lines = new ArrayList<>();
        Scanner inputStream;

        try{
            inputStream = new Scanner(file);

            while(inputStream.hasNext()){
                String line= inputStream.next();
                String[] values = line.split(",");
                lines.add(Arrays.asList(values));
            }

            inputStream.close();
        }catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        
        for (int i = 6; i < 727 ; i++) 
        {
        	
    	Pokemon pokemonToRegister = new Pokemon();
        	
        selected = i ;
        int lineNo = 1;
        
        for(List<String> line: lines) 
        {
            int columnNo = 1;
            
            for (String value: line) 
            {
            	if (columnNo == 1 && lineNo == selected) 
            	{
            		local_id = value;
            	}
            	if (columnNo == 2 && lineNo == selected) 
            	{
            		local_name = value;
            	}
            	if (columnNo == 3 && lineNo == selected) 
            	{
            		local_type_1 = value;
            	}
            	if (columnNo == 4 && lineNo == selected) 
            	{
            		local_type_2 = value;
            	}
            	if (columnNo == 5 && lineNo == selected) 
            	{
            		local_Total = value;
            	}
            	if (columnNo == 6 && lineNo == selected) 
            	{
            		local_HP = value;
            	}
            	if (columnNo == 7 && lineNo == selected) 
            	{
            		local_Attack = value;
            	}
            	if (columnNo == 8 && lineNo == selected) 
            	{
            		local_Defense = value;
            	}
            	if (columnNo == 9 && lineNo == selected) 
            	{
            		local_Sp_Atk = value;
            	}
            	if (columnNo == 10 && lineNo == selected) 
            	{
            		local_Sp_Def = value;
            	}
            	if (columnNo == 11 && lineNo == selected) 
            	{
            		local_Speed = value;
            	}
            	if (columnNo == 12 && lineNo == selected) 
            	{
            		local_Generation = value;
            	}
            	if (columnNo == 13 && lineNo == selected) 
            	{
            		local_Legendary = value;
            	}            	                
                columnNo++;
            }
            lineNo++;
        }
        
        pokemonToRegister.setId(Integer.parseInt(local_id));
        pokemonToRegister.setName(local_name);
        pokemonToRegister.setType_1(local_type_1);
        pokemonToRegister.setType_2(local_type_2);
        pokemonToRegister.setTotal(Integer.parseInt(local_Total));
        pokemonToRegister.setHP(Integer.parseInt(local_HP));
        pokemonToRegister.setAttack(Integer.parseInt(local_Attack));
        pokemonToRegister.setDefense(Integer.parseInt(local_Defense));
        pokemonToRegister.setSp_Atk(Integer.parseInt(local_Sp_Atk));
        pokemonToRegister.setSp_Def(Integer.parseInt(local_Sp_Def));
        pokemonToRegister.setSpeed(Integer.parseInt(local_Speed));
        pokemonToRegister.setGeneration(Integer.parseInt(local_Generation));
        pokemonToRegister.setLegendary(local_Legendary);
        pokemonToRegister.setImageName(local_name.concat(".png").toLowerCase());
        
    	
        selected = i ;
        int lineNo2 = 1;
        //Abilitie_1,Abilitie_2,Abilitie_3,Move_1,Move_2,Move_3,Move 3,Move 5,Move 6,Move 7
        for(List<String> line: lines) 
        {
            int columnNo = 1;
            
            for (String value: line) 
            {
            	if (columnNo == 3 && lineNo2 == selected) 
            	{
            		local_xp = value;
            	}
            	if (columnNo == 4 && lineNo2 == selected) 
            	{
            		local_Height = value;
            	}
            	if (columnNo == 5 && lineNo2 == selected) 
            	{
            		local_Weight = value;
            	}
            	if (columnNo == 6 && lineNo2 == selected) 
            	{
            		local_Abilitie_1 = value;
            	}
            	if (columnNo == 7 && lineNo2 == selected) 
            	{
            		local_Abilitie_2 = value;
            	}
            	if (columnNo == 8 && lineNo2 == selected) 
            	{
            		local_Move_1 = value;
            	}
            	if (columnNo == 9 && lineNo2 == selected) 
            	{
            		local_Move_2 = value;
            	}
            	if (columnNo == 10 && lineNo2 == selected) 
            	{
            		local_Move_3 = value;
            	}
            	if (columnNo == 11 && lineNo2 == selected) 
            	{
            		local_Speed = value;
            	}
            	if (columnNo == 12 && lineNo2 == selected) 
            	{
            		local_Generation = value;
            	}
            	if (columnNo == 13 && lineNo2 == selected) 
            	{
            		local_Legendary = value;
            	}            	                
                columnNo++;
            }
            lineNo2++;
        }
        
        pokemonToRegister.setId(Integer.parseInt(local_id));
        pokemonToRegister.setName(local_name);
        pokemonToRegister.setType_1(local_type_1);
        pokemonToRegister.setType_2(local_type_2);
        pokemonToRegister.setTotal(Integer.parseInt(local_Total));
        pokemonToRegister.setHP(Integer.parseInt(local_HP));
        pokemonToRegister.setAttack(Integer.parseInt(local_Attack));
        pokemonToRegister.setDefense(Integer.parseInt(local_Defense));
        pokemonToRegister.setSp_Atk(Integer.parseInt(local_Sp_Atk));
        pokemonToRegister.setSp_Def(Integer.parseInt(local_Sp_Def));
        pokemonToRegister.setSpeed(Integer.parseInt(local_Speed));
        pokemonToRegister.setGeneration(Integer.parseInt(local_Generation));
        pokemonToRegister.setLegendary(local_Legendary);
        pokemonToRegister.setImageName(local_name.concat(".png").toLowerCase());
        
        pokemonsListed.add(pokemonToRegister);

        }
	}
	
	public Pokemon pokemonSearchByName (String nameForSearch ) {
		
		Pokemon pokemonFound = new Pokemon();
		
		for (int i = 0 ; i <= 720 ; i++ ) {
			
			if (nameForSearch.toLowerCase().equals( pokemonsListed.get(i).getName().toLowerCase() ) ) {
				
			pokemonFound = pokemonsListed.get(i);
			
			}
		}

		return pokemonFound;
	}
	
	public Pokemon getSpecifiedPokemon (int i) {
		
		Pokemon pokemonSpecified = new Pokemon();
		
			pokemonSpecified = pokemonsListed.get(i);

		return pokemonSpecified;
	}
}
