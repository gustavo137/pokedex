import java.awt.EventQueue;
import java.awt.Image;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JButton;
import java.awt.BorderLayout;
import javax.swing.JTextField;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.GroupLayout.Alignment;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;

public class BackgroundDrawer {
	
	JLabel BackGround = new JLabel("");
	String imagesLocation = ("pokedexImages/BackGroundPokedex.gif");
	
	public void drawBackground(JFrame frame) {
		
		BackGround.setBounds(-17, -17, 620, 620);
		frame.getContentPane().add(BackGround);
		Image img = new ImageIcon (this.getClass().getResource(imagesLocation)).getImage();
		BackGround.setIcon(new ImageIcon(img));
		frame.getContentPane().add(BackGround);
		frame.setResizable(false);
		
	}
	
	public void drawSideDecorations(JFrame frame) {
		JLabel SideDecoration = new JLabel("");
		SideDecoration.setBounds(225, 0, 225, 300);
		frame.getContentPane().add(SideDecoration);
		
		String imagesLocation_character = ("pokedexImages/PokedexInterface.png");
		System.out.println(imagesLocation_character);
		Image img_character = new ImageIcon (this.getClass().getResource(imagesLocation_character)).getImage();
		SideDecoration.setIcon(new ImageIcon(img_character));
		frame.getContentPane().add(SideDecoration);
	}
}
