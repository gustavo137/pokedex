import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JButton;
import java.awt.BorderLayout;
import javax.swing.JTextField;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.GroupLayout.Alignment;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class LoginPageInterface extends BackgroundDrawer {

	private JFrame frame;
	private JTextField txtUsername;
	private JPasswordField passwordField;
	LoginManager newManager = new LoginManager();
	Trainer newTrainer = new Trainer();

	public LoginPageInterface() {
		
	}
	
	public void showPage(boolean option) {
		if (option == true) {
			initialize();
			frame.setVisible(true);
		}else
		if (option == false) {
			initialize();
			frame.setVisible(false);
		}
			
	}

	public void initialize() {
		
		frame = new JFrame();
		
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
		        if (JOptionPane.showConfirmDialog(frame, 
		                "Are you sure you want to close this Pokedex?", "Close Pokedex?", 
		                JOptionPane.YES_NO_OPTION,
		                JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION){
		        	    //frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		                System.exit(0);
		            }
			}
		});
		
		frame.setResizable(false);
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblName = new JLabel("Username:");
		lblName.setBounds(67, 79, 83, 15);
		frame.getContentPane().add(lblName);
		
		txtUsername = new JTextField();
		txtUsername.setBounds(158, 77, 114, 19);
		frame.getContentPane().add(txtUsername);
		txtUsername.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("PassWord:");
		lblNewLabel.setBounds(72, 95, 93, 61);
		frame.getContentPane().add(lblNewLabel);
		
		JButton btnRegister = new JButton("Register");
		btnRegister.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.out.println("Loading Registration Page...");
				RegistrationPageInterface newRegistrationPageInterface = new RegistrationPageInterface();
				newRegistrationPageInterface.showPage(true);
				frame.setVisible(false);
			}
		});
		btnRegister.setBounds(158, 183, 117, 25);
		frame.getContentPane().add(btnRegister);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(158, 116, 111, 19);
		frame.getContentPane().add(passwordField);
		
		
		JButton btnSigin = new JButton("Log In");
		btnSigin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String username = txtUsername.getText();
				String password = passwordField.getText(); 
				
				if (username.equals("") || passwordField.equals("")) 
				{
					JOptionPane.showMessageDialog(frame, "Invalid Fields");
				}else if (newManager.readDataFiles(username, password).getName().equals("") == false  ) 
				{
				JOptionPane.showMessageDialog(frame, "Login sucessfull");
				TrainerPageInterface newTrainerPage = new TrainerPageInterface();
				//newTrainerPage.setLocalTrainer(newManager.getTrainer(username));
				newTrainerPage.showPage(true, newManager.readDataFiles(username, password));
				frame.setVisible(false);
				}else{
				System.out.println("account not found");
				JOptionPane.showMessageDialog(frame, "Login Failed - Account not found");
				txtUsername.setText(null);
				passwordField.setText(null);
				}
			}
		});
		btnSigin.setBounds(158, 146, 117, 25);
		frame.getContentPane().add(btnSigin);
		
		JLabel Miniature = new JLabel("");
		Miniature.setBounds(82, 144, 64, 64);
		frame.getContentPane().add(Miniature);
		
		String imagesLocation_characte = ("pokedexImages/FemaleCharacterMiniature.gif");
		System.out.println(imagesLocation_characte);
		Image img_characte = new ImageIcon (this.getClass().getResource(imagesLocation_characte)).getImage();
		Miniature.setIcon(new ImageIcon(img_characte));
		frame.getContentPane().add(Miniature);
		
		JLabel MenuHolder = new JLabel("");
		MenuHolder.setBounds(53, 55, 300, 200);
		frame.getContentPane().add(MenuHolder);
		
		String imagesLocation_character = ("pokedexImages/MenuHolder.png");
		System.out.println(imagesLocation_character);
		Image img_character = new ImageIcon (this.getClass().getResource(imagesLocation_character)).getImage();
		MenuHolder.setIcon(new ImageIcon(img_character));
		frame.getContentPane().add(MenuHolder);
		
		
		this.drawBackground(frame);

		frame.setVisible(true);

	}
}
